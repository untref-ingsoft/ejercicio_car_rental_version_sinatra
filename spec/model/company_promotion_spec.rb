require 'spec_helper'

describe CompanyPromotion do
  let(:some_date) { Date.parse('20190101') }

  it 'should apply 5% to enterprises' do
    rent = RentPerHour.new(some_date, some_date, '26223334446', 1)

    result = CompanyPromotion.new.apply(rent)
    expect(result).to eq 95
  end

  it 'should not apply discount to not enterprises' do
    rent = RentPerHour.new(some_date, some_date, '20223334446', 1)

    result = CompanyPromotion.new.apply(rent)
    expect(result).to eq 100
  end
end
