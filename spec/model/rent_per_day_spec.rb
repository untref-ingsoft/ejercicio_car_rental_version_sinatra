require 'spec_helper'

describe RentPerDay do
  let(:some_date) { Date.parse('20190101') }

  it 'amount for 1 day should be 2000' do
    rent = RentPerDay.new(some_date, some_date, '20223334446', 1)
    expect(rent.amount).to eq 2000
  end

  it 'amount for 2 days should be 4000' do
    rent = RentPerDay.new(some_date, Date.parse('20190102'), '20223334446', 2)
    expect(rent.amount).to eq 4000
  end
end
