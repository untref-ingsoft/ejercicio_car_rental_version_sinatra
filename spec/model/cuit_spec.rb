require 'spec_helper'

describe Cuit do
  it 'should return true for is_company? if cuit number starts with 26' do
    cuit = Cuit.new('26123456789')
    expect(cuit.is_company?).to eq(true)
  end

  it 'should return false for is_company? if cuit number starts with a value different than 26' do
    cuit = Cuit.new('20123456789')
    expect(cuit.is_company?).to eq(false)
  end
end
