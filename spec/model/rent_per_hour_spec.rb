require 'spec_helper'

describe RentPerHour do
  let(:some_date) { Date.parse('20190101') }

  it 'amount for 1 hours should be 100' do
    rent = RentPerHour.new(some_date, some_date, '20223334446', 1)
    expect(rent.amount).to eq 100
  end

  it 'amount for 2 hours should be 200' do
    rent = RentPerHour.new(some_date, some_date, '20223334446', 2)
    expect(rent.amount).to eq 200
  end
end
