# Car Rental versión Web API

Implementar la versión web API de la aplicación Car Rental. Para ello se deben exponer los siguientes endpoints:

## Nuevo alquiler por día: `POST /rent/per_day`

### Parámetros:
  * `date_from`: fecha de inicio del alquiler en formato `dd/mm/yyyy`
  * `date_to`: fecha de devolución del rodado en formato `dd/mm/yyyy`
  * `cuit`: número de CUIT del cliente
  * `days`: cantidad de días por las que se alquiló

### Ejemplo:
`curl -X POST http://localhost:4567/rent/per_day -d 'date_from=01/01/2021&date_to=01/01/2021&cuit=20123456789&days=1'`

## Nuevo alquiler por hora: `POST /rent/per_hour`

### Parámetros:
  * `date_from`: fecha de inicio del alquiler en formato `dd/mm/yyyy`
  * `date_to`: fecha de devolución del rodado en formato `dd/mm/yyyy`
  * `cuit`: número de CUIT del cliente
  * `hours`: cantidad de horas por las que se alquiló

### Ejemplo:
`curl -X POST http://localhost:4567/rent/per_hour -d 'date_from=01/01/2021&date_to=01/01/2021&cuit=20123456789&hours=12'`
