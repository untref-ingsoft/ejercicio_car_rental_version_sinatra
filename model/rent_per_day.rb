class RentPerDay
  COST_PER_DAY = 2000

  attr_reader :rent_fom, :rent_to, :cuit, :days

  def initialize(date_from, date_to, cuit, days)
    @rent_from = date_from
    @rent_to = date_to
    @cuit = Cuit.new(cuit)
    @days = days
  end

  def amount
    COST_PER_DAY * @days
  end

  def late?
    false
  end
end
