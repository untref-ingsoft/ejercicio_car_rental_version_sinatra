class Cuit
  COMPANY_PREFIX = '26'.freeze

  attr_reader :cuit_number

  def initialize(cuit_number)
    @cuit_number = cuit_number
  end

  def is_company?
    cuit_number.start_with?(COMPANY_PREFIX)
  end
end
