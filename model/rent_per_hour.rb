class RentPerHour
  COST_PER_HOUR = 100

  attr_reader :rent_fom, :rent_to, :cuit, :hours

  def initialize(date_from, date_to, cuit, hours)
    @rent_from = date_from
    @rent_to = date_to
    @cuit = Cuit.new(cuit)
    @hours = hours
  end

  def amount
    COST_PER_HOUR * @hours
  end

  def late?
    @rent_from != @rent_to
  end
end
