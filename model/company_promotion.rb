class CompanyPromotion
  DISCOUNT = 0.05

  def apply(rent)
    return rent.amount - (rent.amount * DISCOUNT) if rent.cuit.is_company?

    rent.amount
  end
end
