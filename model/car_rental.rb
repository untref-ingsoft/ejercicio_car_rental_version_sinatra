require 'Date'
require_relative './rent_per_day'
require_relative './rent_per_hour'
require_relative './company_promotion'

class CarRental
  RENT_TYPE_PER_HOUR = 'h'.freeze
  RENT_TYPE_PER_DAY = 'd'.freeze

  WEDNESDAY_DISCOUNT = 50
  LATE_OVERCHARGE_FACTOR = 2

  RENT_TYPES = {
    RENT_TYPE_PER_HOUR => RentPerHour,
    RENT_TYPE_PER_DAY => RentPerDay
  }.freeze

  def self.calculate_billing_for(date_from, date_to, cuit, rent_type, rent_params)
    rent = RENT_TYPES[rent_type].new(date_from, date_to, cuit, rent_params.to_i)
    amount = apply_prize_modifiers(rent)

    amount -= WEDNESDAY_DISCOUNT if rent.date_from.wednesday?
    amount *= LATE_OVERCHARGE_FACTOR if rent.late?

    amount
  end

  def self.apply_prize_modifiers(rent)
    CompanyPromotion.new.apply(rent)
  end
end
